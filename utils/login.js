module.exports = {
  isLoggedIn : function(req,res,next){
    var jwt = require('jsonwebtoken');
    var jwtSecret = require('../config/jwt');
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(token){
      jwt.verify(token, jwtSecret.secret, function(err,decoded){
        if(err){
          return res.status(401).json({message:'Failed to authenticate token', err:err});
        }
        req.user = decoded;
        next();
        return;
      })
    }else{
      return res.status(403).json({message:'No token was provided'});
    }
  }
}
