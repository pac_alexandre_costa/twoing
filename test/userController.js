//See https://mochajs.org/#getting-started for more details about mocha
var assert = require('assert');
var mongoose = require('mongoose');
var environment = require('../utils/commons').config();
var User = require('../models/user');
var UserController = require('../controllers/user');


let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);

const app = require('../app');

before(function (done) {
    mongoose.connect(environment.urlDB, function (err) {
        if (err) {
            done(err);
            return;
        }
        done();
    });
});

beforeEach(function (done) {
    // let's ensure the test database is empty before test
    User.remove({}, done);
})

after(function () {
    // disconnect to avoid hanged server
    mongoose.disconnect();
});


describe('#UserController', function () {
    describe('#getUsers', function () {
        it('should retrieve users without error', function (done) {
            UserController.getUsers(function (err, result) {
                if (err) {
                    done(new Error(err.error));
                    return;
                }
                done();
            });
        })
    })
})
