//See https://mochajs.org/#getting-started for more details about mocha
var assert = require('assert');
var mongoose = require('mongoose');
var environment = require('../utils/commons').config();
var User = require('../models/user');
var UserController = require('../controllers/user');


let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);

const app = require('../app');

before(function (done) {
    mongoose.connect(environment.urlDB, function (err) {
        if (err) {
            done(err);
            return;
        }
        done();
    });
});

beforeEach(function (done) {
    // let's ensure the test database is empty before test
    User.remove({}, done);
})

after(function () {
    // disconnect to avoid hanged server
    mongoose.disconnect();
});


describe('#account api', () => {
    describe('/POST signup', () => {
        it('it should create an account', (done) => {
            chai.request(app)
                .post('/api/v1/account/signup')
                .send({
                    'name': 'test',
                    'email': 'test@test.com',
                    'password': 'test'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    describe('/POST signup', () => {
        it('it should not create an account with empty name', (done) => {
            chai.request(app)
                .post('/api/v1/account/signup')
                .send({
                    'email': 'test@test.com',
                    'password': 'test'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
    });

    describe('/POST signup', () => {
        it('it should not create an account with email already used', (done) => {
            chai.request(app)
                .post('/api/v1/account/signup')
                .send({
                    'name': 'test',
                    'email': 'test@test.com',
                    'password': 'test'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    chai.request(app)
                        .post('/api/v1/account/signup')
                        .send({
                            'name': 'test',
                            'email': 'test@test.com',
                            'password': 'test'
                        })
                        .end((err, res) => {
                            res.should.have.status(400);
                            done();
                        });
                });
        });
    });

    describe('/POST login', () => {
        it('it should login', (done) => {
            chai.request(app)
                .post('/api/v1/account/signup')
                .send({
                    'name': 'test',
                    'email': 'test@test.com',
                    'password': 'test'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    chai.request(app)
                        .post('/api/v1/account/login')
                        .send({
                            'email': 'test@test.com',
                            'password': 'test'
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            done();
                        });
                });
        });
    });

    describe('/POST login', () => {
        it('it should not login without password', (done) => {
            chai.request(app)
                .post('/api/v1/account/signup')
                .send({
                    'name': 'test',
                    'email': 'test@test.com',
                    'password': 'test'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    chai.request(app)
                        .post('/api/v1/account/login')
                        .send({
                            'email': 'test@test.com'
                        })
                        .end((err, res) => {
                            res.should.have.status(400);
                            done();
                        });
                });
        });
    });

    describe('/POST login', () => {
        it('it should not login with wrong password', (done) => {
            chai.request(app)
                .post('/api/v1/account/signup')
                .send({
                    'name': 'test',
                    'email': 'test@test.com',
                    'password': 'test'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    chai.request(app)
                        .post('/api/v1/account/login')
                        .send({
                            'email': 'test@test.com',
                            'password': 'test_wrong'
                        })
                        .end((err, res) => {
                            res.should.have.status(400);
                            done();
                        });
                });
        });
    });

    describe('/POST login', () => {
        it('it should not login with inexistent user', (done) => {
            chai.request(app)
                .post('/api/v1/account/login')
                .send({
                    'name': 'test',
                    'email': 'test@test.com',
                    'password': 'test'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
    });

});
