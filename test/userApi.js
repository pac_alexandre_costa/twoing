//See https://mochajs.org/#getting-started for more details about mocha
var assert = require('assert');
var mongoose = require('mongoose');
var environment = require('../utils/commons').config();
var User = require('../models/user');
var UserController = require('../controllers/user');


let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);

const app = require('../app');
let token = '';
let id = '';
before(function (done) {
    mongoose.connect(environment.urlDB, function (err) {
        if (err) {
            done(err);
            return;
        }
        // let's ensure the test database is empty before test
        User.remove({}, () => {
            chai.request(app)
                .post('/api/v1/account/signup')
                .send({
                    'name': 'test',
                    'email': 'test@test.com',
                    'password': 'test'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    token = res.body.token;
                    User.findOne({}, function (err, result) {
                        if (err) {
                            done(err);
                            return;
                        }
                        id = result._id;
                        done();
                    });

                });
        });
    });
});

after(function () {
    // disconnect to avoid hanged server
    mongoose.disconnect();
});


describe('#user api', () => {
    describe('/GET users', () => {
        it('it should get an existing user', (done) => {
            chai.request(app)
                .get('/api/v1/users/'+id)
                .set('x-access-token', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    describe('/POST users/message', () => {
        it('it should post an message', (done) => {
            chai.request(app)
                .post('/api/v1/users/'+id+'/messages')
                .set('x-access-token', token)
                .send({'content':'any test message'})
                .end((err, res) => {
                    res.should.have.status(200);                    
                    res.body.should.have.property('_id');
                    done();
                });
        });
    });
});
