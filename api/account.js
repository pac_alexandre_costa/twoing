// app/routes.js
module.exports = function(app, passport) {

    var express = require('express')
    var router = express.Router()

    var User = require('../models/user')

    var jwt    = require('jsonwebtoken') // used to create, sign, and verify tokens
    var jwtSecret = require('../config/jwt.js')


    router.post('/login', function (req,res,next){

        passport.authenticate('local-login' , {session : false, failureFlash:false} , function(err, user, info){
            if(err){
                res.status(400).send(info);
            }
            else if(!user){
                res.status(400).send(info)
            }else {
                var userInfo = user.local;
                userInfo.password = undefined;
                var token = jwt.sign(user.toObject(), jwtSecret.secret, {expiresIn: 120 * 24})
                res.json({success:true, message:'Enjoy your tokens', token:token, user:userInfo})
            }
        })(req, res, next)
    });
    router.post('/signup', function(req,res,next){
        passport.authenticate('local-signup' , {session : false, failureFlash:false} , function(err, user, info){
            if(err || !user){
                res.status(400).send({message:err, err:info})
            }else {
                var token = jwt.sign(user.toObject(), jwtSecret.secret, {expiresIn: 60 * 24})
                res.json({success:true, message:'Enjoy your account', token:token})
            }
        })(req, res, next)
    })
    return router;
};
