var express = require('express');
var router = express.Router();
var controller = require('../controllers/message');

router.get('/', function(req,res){
  var userId = req.userId;
  controller.getMessagesByUser(userId, function(err, result){
    if(err){
      console.log('deu ruim',err);
      res.status(400).send(err);
    }else {
      console.log('deu certo', err, result);
      res.status(200).send(result);
    }
  })
});

router.get('/:messageId', function(req,res){
  var messageId = req.params.messageId;
  controller.getMessagesById(messageId, function(err, result){
    if(err){
      console.log('deu ruim',err);
      res.status(400).send(err);
    }else {
      console.log('deu certo', err, result);
      res.status(200).send(result);
    }
  })
});


router.post('/:userId', function(req,res){
  var userId = req.params.userId
});

module.exports = router;
