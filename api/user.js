var express = require('express');
var router = express.Router();
var userController = require('../controllers/user');
var messageController = require('../controllers/message');

router.get('/:userId', function(req,res){
  var userId = req.params.userId;
  userController.getUserById(userId, function(err, result){
    if(err){
      res.status(400).send(err);
    }else {
      res.status(200).send(result);
    }
  })
});

router.get('/:userId/messages', function(req,res){
  var userId = req.params.userId;
  messageController.getMessagesByUserId(userId, function(err, result){
    if(err){
      console.log('deu ruim',err);
      res.status(400).send(err);
    }else {
      console.log('deu certo', err, result);
      res.status(200).send(result);
    }
  })
});

router.post('/:userId/messages', function(req,res){
  messageController.createMessage(req.params.userId, req.body.content,  function(err, result){
    if(err){
      res.status(400).send(err);
    }else {
      res.status(200).send(result);
    }
  })
});


router.get('/:userId/friends', function(req,res){
  var userId = req.params.userId;
  userController.getFriends(userId, function(err, result){
    if(err){
      res.status(400).send(err);
    }else {
      res.status(200).send(result);
    }
  })
});

router.post('/:userId/friends/add/:friendId', function(req,res){
  var userId = req.params.userId;
  var friendId = req.params.friendId;
  userController.addFriend(userId, friendId, function(err, result){
    if(err){
      res.status(400).send(err);
    }else {
      res.status(200).send(result);
    }
  })
});

module.exports = router;
