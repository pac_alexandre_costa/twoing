var mongoose = require('mongoose');

var messageSchema = mongoose.Schema({
    userId : { type: mongoose.Schema.ObjectId, ref: 'User', required: true}
    , thread : [ {type: mongoose.Schema.ObjectId , ref: 'Message' }]
    , content : {type:String, required:true}
    , date : { type: Date, default: Date.now}
    , deleted : {type : Boolean }
});

module.exports = mongoose.model('Message', messageSchema);
