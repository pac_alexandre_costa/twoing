var mongoose = require('mongoose');
var bcrypt  = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
    local : {
      name: { type : String, required:true},
      email: { type : String, required:true},
      password: { type : String, required:true},
      public : Boolean,
      profile : { type: String , enum:['user','admin'], default:'user'},
      friends : [{friendId: {type: mongoose.Schema.ObjectId, ref:'User'}
                  , status: {type: String,  enum:['pending','accepted', 'blocked']}
                }]
    }
  }
);

userSchema.methods.generateHash = function(password){
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password){
  return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);
