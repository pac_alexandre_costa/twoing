var User = require('../models/user');
var Message = require('../models/message');

exports.getUsers = function(cb){
  User.find(function(err,result){
    return cb(err,result);
  })
};

exports.getUserById = function(userId, cb){
  User.findById(userId ,function(err,result){
    return cb(err,result);
  })
};

exports.getFriends = function(userId, cb){
  User.findById(userId)
  .select('local.name local.friends')
  .populate('local.friends','local.name')
  .exec(function(err,result){
    return cb(err,result ? result.local.friends : result);
  })
};

exports.addFriend = function(userId, friendId, cb){
  var friend = {friendId : friendId, status:'pending'};
  User.findByIdAndUpdate(userId, {$push : {'local.friends': friend}} , {new:true}, function(err,result){
    return cb(err,result);
  })
};
