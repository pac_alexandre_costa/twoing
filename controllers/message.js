var User = require('../models/user');
var Message = require('../models/message');


exports.getMessagesByUserId = function(userId,cb){
  Message.find({userId : userId}, function(err, messages){
    return cb(err, messages);
  })
};


exports.getMessagesById = function(messageId,cb){
  Message.findById({messageId}, function(err, message){
    return cb(err, message);
  })
};

exports.createMessage = function(userId, content, cb){
  var newMessage = new Message({userId: userId, content:content});
  newMessage.save(function(err, savedMessage){
    return cb(err,savedMessage);
  })
};

exports.deleteMessage = function(messageId, cb){
  var newMessage = new Message({userId: userId, content:content});
  newMessage.save(function(err, savedMessage){
    return cb(err,savedMessage);
  })
};
